import React from 'react';
import { bind } from 'decko';
import { observer } from 'mobx-react';
import Dropzone from 'react-dropzone';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import editorStore, { ModalType } from './store/editor';
import './style.scss';
import { action } from 'mobx';

@observer
class Modal extends React.Component {
  state = {
    warn: false,
    background: null,
    crop: {
      unit: '%',
      width: 100,
      aspect: 780.0 / 540.0,
    },
  };

  @bind
  dropReject() {
    this.setState({ ...this.state, warn: true });
  }

  @bind
  onDrop([file]) {
    if (!file) return;

    const reader = new FileReader();
    reader.addEventListener('load', () =>
      this.setState({
        ...this.state,
        background: reader.result,
        crop: { ...this.state.crop, aspect: editorStore.backgroundAspectRatio },
      }),
    );
    reader.readAsDataURL(file);
  }

  @bind
  onCropChange(crop) {
    this.setState({ ...this.state, crop });
  }

  @bind
  setBackground() {
    const { crop } = this.state;
    const image = this.imageRef;

    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height,
    );

    canvas.toBlob((blob) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        editorStore.addNewBackground(reader.result);
        this.closeModal();
      };
    }, 'image/jpeg');
  }

  @bind
  @action
  closeModal() {
    editorStore.modalOpen = null;
    this.setState({ ...this.state, background: null });
  }

  render() {
    if (!editorStore.modalOpen) return null;
    const { warn, background, crop } = this.state;

    return (
      <div className="modal-background">
        <div className="modal">
          <div className="modal-content">
            {editorStore.modalType === ModalType.BACKGROUND_UPLOAD &&
              (background ? (
                <>
                  <ReactCrop
                    src={background}
                    crop={crop}
                    onImageLoaded={(image) => (this.imageRef = image)}
                    onChange={this.onCropChange}
                  />
                  <div className="actions">
                    <button className="finish-crop-button" onClick={this.setBackground}>
                      Finish
                    </button>
                    <button className="cancel-crop-button" onClick={this.closeModal}>
                      Cancel
                    </button>
                  </div>
                </>
              ) : (
                <Dropzone
                  onDropRejected={this.dropReject}
                  onDropAccepted={this.onDrop}
                  accept="image/jpeg"
                >
                  {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps({ className: 'dropzone' })}>
                      <input {...getInputProps()} />
                      {warn ? (
                        <p className="warn">Please choose jpg image format</p>
                      ) : (
                        <p>Drag & drop an image</p>
                      )}
                    </div>
                  )}
                </Dropzone>
              ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;

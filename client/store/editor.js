import { observable, action } from 'mobx';
import { bind } from 'decko';

export const ModalType = {
  BACKGROUND_UPLOAD: 1,
  PHOTO_UPLOAD: 2,
};

class EditorStore {
  @observable selectedElement = {};
  @observable modalOpen = false;
  @observable modalType = null;
  @observable backgroundAspectRatio = 0;

  @observable changes = [];
  @observable currentPage = 1;

  setText = () => {};
  resetText = () => {};
  applyText = () => {};
  rejectText = () => {};
  changeBackground = () => {};
  addNewBackground = () => {};

  @bind
  @action
  reset() {
    this.selectedElement = {};
    this.modalOpen = false;
    this.modalType = null;
    this.backgroundAspectRatio = 0;

    this.changes = [];
    this.currentPage = 1;

    this.setText = () => {};
    this.resetText = () => {};
    this.applyText = () => {};
    this.rejectText = () => {};
    this.changeBackground = () => {};
    this.addNewBackground = () => {};
  }
}

export default new EditorStore();

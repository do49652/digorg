import React from 'react';
import ReactDOM from 'react-dom';
import interact from 'interactjs';
import reactable from 'reactablejs';
import { bind } from 'decko';

import editorStore from '../../store/editor';
import { observer } from 'mobx-react';

const DivWithText = reactable(
  ({ getRef, id, text, left, top, editMode, onBlur, onClick, selected }) => (
    <div
      ref={getRef}
      id={id}
      onKeyDown={onBlur}
      onPointerDown={onClick}
      contentEditable={editMode ? 'true' : 'false'}
      style={{
        left,
        top,
        position: 'absolute',
        touchAction: 'none',
        whiteSpace: 'nowrap',
        color: 'black',
        // border: selected ? '2px solid yellow' : editMode ? '1px dashed gray' : null,
        border: selected || editMode ? '1px dashed gray' : null,
      }}
      dangerouslySetInnerHTML={{ __html: text }}
    />
  ),
);

@observer
class TextBox extends React.Component {
  state = {};

  componentDidMount() {
    const { left, top } = this.props;
    this.setState({ left, top });
  }

  @bind
  onDragMove({ dx, dy }) {
    const { editMode } = this.props;
    if (!editMode) return;

    let { left, top } = this.state;

    left = `${parseInt(left.replace(/[^0-9.]/g, '')) + dx}px`;
    top = `${parseInt(top.replace(/[^0-9.]/g, '')) + dy}px`;

    this.setState({ left, top });
  }

  @bind
  onDragEnd() {
    const { id, onDragEnd, onEditEnd } = this.props;
    const { left, top } = this.state;
    onEditEnd(id, ReactDOM.findDOMNode(this).innerHTML);
    onDragEnd(id, left, top);
  }

  @bind
  onEditEnd() {
    const { id, onEditEnd } = this.props;
    onEditEnd(id, ReactDOM.findDOMNode(this).innerHTML);
  }

  render() {
    const { id, text, editMode, onClick } = this.props;
    const { left, top } = this.state;

    const selected = `${editorStore.selectedElement.i}` === id.replace(/[^0-9]/g, '');

    return (
      <DivWithText
        // draggable={{
        //   onmove: this.onDragMove,
        //   onend: this.onDragEnd,
        //   modifiers: [
        //     interact.modifiers.snap({
        //       targets: [interact.createSnapGrid({ x: 4, y: 4 })],
        //       range: Infinity,
        //       relativePoints: [{ x: 0, y: 0 }],
        //     }),
        //   ],
        // }}
        id={id}
        editMode={selected}
        text={text}
        left={left}
        top={top}
        onBlur={this.onEditEnd}
        onClick={onClick}
        selected={selected}
      />
    );
  }
}

export default TextBox;

import React from 'react';

class UploadForm extends React.Component {
  render() {
    return (
      <form action="/upload" method="post" encType="multipart/form-data">
        <input type="file" name="file" defaultValue="" />
        <button type="submit">Upload</button>
      </form>
    );
  }
}

export default UploadForm;

import React from 'react';
import { observer } from 'mobx-react';
import TextBox from '../TextBox';
import editorStore from '../../store/editor';

class Slide extends React.Component {
  render() {
    const { page, background, onDragEnd, onEditEnd, editMode } = this.props;

    return (
      <div style={{ position: 'absolute', transform: 'scale(1.4)' }}>
        <img
          style={{ pointerEvents: 'none' }}
          src={background}
          width={page.background.width}
          height={page.background.height}
        />

        {page.texts.map(({ text, left, top }, i) => (
          <TextBox
            key={`${left}${top}`}
            id={`text-${i}`}
            text={text}
            left={left}
            top={top}
            // editMode={editMode}
            onDragEnd={onDragEnd}
            onEditEnd={onEditEnd}
            onClick={() => {
              editorStore.changeBackground();
              editorStore.selectedElement = { page, i };
              // this.forceUpdate();
            }}
          />
        ))}
      </div>
    );
  }
}

export default Slide;

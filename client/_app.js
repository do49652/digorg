import '@babel/polyfill';
import 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import Sidebar from './Sidebar';
import Modal from './Modal';

function renderApp() {
  ReactDOM.render(<App />, document.getElementById('app'));
  ReactDOM.render(<Sidebar />, document.getElementById('sidebar'));
  ReactDOM.render(<Modal />, document.getElementById('modal'));
}

renderApp();
if (module.hot) {
  module.hot.accept(renderApp);
}

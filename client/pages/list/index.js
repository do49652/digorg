import React from 'react';

class List extends React.Component {
  state = {
    files: [],
  };

  async componentDidMount() {
    const res = await fetch('/files');
    this.setState({ files: await res.json() });
  }

  render() {
    const { files } = this.state;
    const { setGuid } = this.props;

    return (
      <>
        <ul>
          {files.map((file) => (
            <li>
              <a href="#" onClick={() => setGuid(file.guid)}>
                {file.name}
              </a>
            </li>
          ))}
        </ul>
      </>
    );
  }
}

export default List;

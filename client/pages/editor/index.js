import React from 'react';
import { bind } from 'decko';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { debounce, throttle } from 'throttle-debounce';
import Slide from '../../components/Slide';
import editorStore from '../../store/editor';

@observer
class Editor extends React.Component {
  queue = [];
  state = {
    data: null,
    currentPage: 1,
    editMode: false,
    saving: false,
    saved: true,
    backgroundAdded: false,
  };

  async componentDidMount() {
    const { guid } = this.props;
    this.setState({ data: await (await fetch(`/p/${guid}`)).json() }, () => {
      const { data } = this.state;
      this.data = JSON.parse(JSON.stringify(data));
      try {
        editorStore.backgroundAspectRatio =
          parseFloat(data.pages[1].background.width) / parseFloat(data.pages[1].background.height);
      } catch {}
      // try {
      //   data.changes.forEach((change) => {
      //     try {
      //       data.pages[change.page].texts[change.textIndex].text = change.text;
      //     } catch {}
      //   });
      // } catch {}
      try {
        editorStore.changes = data.changes;
        this.forceUpdate();
      } catch {}
    });

    document
      .getElementById('app')
      .addEventListener('pointerdown', () => (editorStore.selectedElement = {}));

    window.addEventListener('keydown', this.typingTrigger);

    setTimeout(() => {
      editorStore.setText = this.setText;
      editorStore.resetText = this.resetText;
      editorStore.applyText = this.applyText;
      editorStore.rejectText = this.rejectText;
    }, 1000);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.typingTrigger);
  }

  @bind
  typingTrigger() {
    this.saveLastSlideDebounced();
  }

  @bind
  onDragEnd(id, left, top) {
    const { currentPage, data } = this.state;
    id = parseInt(id.replace(/[^0-9]/g, ''));
    data.pages[currentPage].texts[id].left = left;
    data.pages[currentPage].texts[id].top = top;
    this.setState({ ...this.state, data, saved: false });
  }

  @bind
  onEditEnd(id, text = 'Empty') {
    const { currentPage, data } = this.state;
    const innerText = text.replace(/<(.|\n)*?>/g, '');
    id = parseInt(id.replace(/[^0-9]/g, ''));
    this.setState({ ...this.state, saved: false }, () => {
      this.sendChangeDebounced(data.guid, currentPage, id)(innerText ? text : 'Empty');
    });
  }

  @bind
  sendChange(guid, currentPage, i, text) {
    this.queue.push({ currentPage, id: i, text });
    fetch(`/change/${guid}`, {
      method: 'post',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        page: currentPage,
        textIndex: i,
        text,
      }),
    });
  }

  debounceFuncs = {};
  sendChangeDebounced = (guid, currentPage, id) => {
    let func = this.debounceFuncs[`${guid}${currentPage}${id}`];
    if (!func) {
      func = debounce(3000, this.sendChange.bind(null, guid, currentPage, id));
      this.debounceFuncs[`${guid}${currentPage}${id}`] = func;
    }
    return func;
  };

  @bind
  sendComment(guid, currentPage, i, text) {
    fetch(`/comment/${guid}`, {
      method: 'post',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        page: currentPage,
        textIndex: i,
        text,
      }),
    });
  }

  sendCommentDebounced = (guid, currentPage, id) => {
    let func = this.debounceFuncs[`${guid}${currentPage}${id}`];
    if (!func) {
      func = debounce(1000, this.sendComment.bind(null, guid, currentPage, id));
      this.debounceFuncs[`${guid}${currentPage}${id}`] = func;
    }
    return func;
  };

  @bind
  changeSlide(by) {
    const { currentPage, data } = this.state;

    if (by === -1) {
      if (currentPage === 1) return;
      this.setState(
        { ...this.state, currentPage: currentPage - 1 },
        () => ((editorStore.currentPage = this.state.currentPage), this.saveLastSlide()),
      );
    } else if (by === 1) {
      if (currentPage === Math.max(...Object.keys(data.pages))) return;
      this.setState(
        { ...this.state, currentPage: currentPage + 1 },
        () => ((editorStore.currentPage = this.state.currentPage), this.saveLastSlide()),
      );
    }

    editorStore.changeBackground();
  }

  @bind
  saveLastSlide() {
    const { data } = this.state;
    for (const { currentPage, id, text } of this.queue) {
      data.pages[currentPage].texts[id].text = text;
      editorStore.changes.push({
        page: currentPage,
        textIndex: id,
        text,
      });
    }
    this.queue = [];
    this.setState({ ...this.state, data });
  }

  saveLastSlideDebounced = debounce(3000, this.saveLastSlide);

  @bind
  addNewTextBox() {
    const { currentPage, data } = this.state;

    data.pages[currentPage].texts.push({
      text: `<span style="font-size:20px;vertical-align:baseline;color:rgba(0,0,0,1);">Empty</span>`,
      left: '400px',
      top: '0px',
    });
    this.setState({ ...this.state, data, saved: false, editMode: true });
  }

  @bind
  setText(i, text) {
    const { currentPage, data } = this.state;
    data.pages[currentPage].texts[i].text = text;
    this.setState({ ...this.state, data, saved: false });
  }

  @bind
  resetText(i) {
    const { currentPage, data } = this.state;
    data.pages[currentPage].texts[i].text = this.data.pages[currentPage].texts[i].text;
    this.setState({ ...this.state, data, saved: false });
  }

  @bind
  applyText(i, text) {
    const { currentPage, data } = this.state;
    this.data.pages[currentPage].texts[i].text = text;
    this.setText(i, text);

    fetch(`/change/${data.guid}`, {
      method: 'put',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        page: currentPage,
        textIndex: i,
        text,
      }),
    });
  }

  @bind
  rejectText(i, text) {
    const { currentPage, data } = this.state;
    this.resetText(i);

    fetch(`/change/${data.guid}`, {
      method: 'delete',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        page: currentPage,
        textIndex: i,
        text,
      }),
    });
  }

  @bind
  toggleEditMode() {
    const { editMode } = this.state;
    this.setState({ ...this.state, editMode: !editMode });
  }

  @bind
  insertNewSlide() {
    const { currentPage, data } = this.state;

    data.pages.push({});
    for (let i = data.pages.length - 2; i > currentPage; i--) data.pages[i + 1] = data.pages[i];

    data.pages[currentPage + 1] = {
      background: Object.assign({}, data.pages[currentPage].background),
      texts: [],
    };

    this.setState({
      ...this.state,
      data,
      editMode: false,
      saved: false,
      currentPage: currentPage + 1,
    });
  }

  @bind
  removeSlide() {
    const { currentPage, data } = this.state;
    if (!currentPage) return;

    for (let i = currentPage; i < data.pages.length; i++) data.pages[i] = data.pages[i + 1];
    data.pages.pop();

    this.setState({
      ...this.state,
      data,
      editMode: false,
      saved: false,
      currentPage: data.pages.length === currentPage ? currentPage - 1 : currentPage,
    });
  }

  @bind
  changeBackground(backgroundId) {
    const { currentPage, data } = this.state;
    data.pages[currentPage].background.id = backgroundId;
    this.setState({ ...this.state, data: data, saved: false });
  }

  @bind
  async addNewBackground(bg) {
    const { currentPage, data } = this.state;
    const newId = `${Math.max(...Object.keys(data.backgrounds).map((k) => parseInt(k))) + 1}`;
    data.backgrounds[newId] = bg;
    data.pages[currentPage].background.id = newId;
    await new Promise((resolve) =>
      this.setState({ ...this.state, data: data, saved: false, backgroundAdded: true }, resolve),
    );

    editorStore.changeBackground();
    return {
      backgrounds: toJS(this.state.data.backgrounds),
      currentPage: newId,
    };
  }

  // @bind
  // async save() {
  //   this.setState({ ...this.state, saving: true, saved: false });
  //   const { guid } = this.props;

  //   const res = await fetch(`/s/${guid}`, {
  //     method: 'post',
  //     headers: { accept: 'application/json', 'content-type': 'application/json' },
  //     body: JSON.stringify({
  //       pages: this.state.data.pages,
  //       ...(this.state.backgroundAdded ? { backgrounds: this.state.data.backgrounds } : {}),
  //     }),
  //   });

  //   const { isSuccess, message } = await res.json();
  //   if (!isSuccess) alert('Did not save ' + message);
  //   this.setState({ ...this.state, saving: false, saved: true });
  // }

  @bind
  exit() {
    const { exitEditor } = this.props;
    editorStore.reset();
    exitEditor();
  }

  render() {
    const { currentPage, data, editMode, saving, saved } = this.state;

    return (
      <>
        {!currentPage ? (
          <h1>Hello World</h1>
        ) : (
          data &&
          data.pages && (
            <Slide
              // editMode={editMode}
              page={data.pages[currentPage]}
              background={data.backgrounds[data.pages[currentPage].background.id]}
              onDragEnd={this.onDragEnd}
              onEditEnd={this.onEditEnd}
            />
          )
        )}

        <div className="slide-controls">
          <div className="mode-control">
            {/* <a onClick={this.toggleEditMode}>
              Edit mode:{' '}
              <span style={{ color: editMode ? 'green' : 'red' }}>{editMode ? 'on' : 'off'}</span>
            </a>
            <a onClick={this.addNewTextBox}>Add textbox</a>
            <a onClick={this.insertNewSlide}>Insert slide</a>
            <a onClick={this.removeSlide}>Remove slide</a>
            <a
              onClick={() =>
                editorStore.changeBackground(
                  toJS(data.backgrounds),
                  data.pages[currentPage].background.id,
                  this.changeBackground,
                  this.addNewBackground,
                )
              }
            >
              Set background
            </a> */}

            {/* <a style={{ marginLeft: 'auto' }} onClick={this.save}>
              {saved ? 'Saved' : 'Save'}
            </a> */}
            <a style={{ marginLeft: 'auto' }} onClick={this.exit}>
              Exit
            </a>
          </div>

          <div className="navigate-control left" onClick={() => this.changeSlide(-1)}>
            {'<'}
          </div>

          <div className="navigate-control right" onClick={() => this.changeSlide(1)}>
            {'>'}
          </div>
        </div>

        {saving && (
          <div className="overlay">
            <h1>Saving...</h1>
          </div>
        )}
      </>
    );
  }
}

export default Editor;

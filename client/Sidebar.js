import React from 'react';
import { observer } from 'mobx-react';
import { bind } from 'decko';
import { CompactPicker } from 'react-color';
import './style.scss';

import editorStore, { ModalType } from './store/editor';
import { action } from 'mobx';

@observer
class Sidebar extends React.Component {
  state = {
    changeBackground: false,
    backgrounds: [],
    displayColorPicker: false,
  };

  componentDidMount() {
    editorStore.changeBackground = this.changeBackground;
  }

  @bind
  resizeFont(i, textElement, by) {
    try {
      let size = parseInt(textElement.text.replace(/^.*font-size:/, '').replace(/px.*$/, ''));
      if (!size) size = 2;
      size += by;
      textElement.text = textElement.text.replace(/font-size:[0-9]+px/, `font-size:${size}px`);
      editorStore.setText(i, textElement.text);
    } catch {}
  }

  @bind
  recolorFont(i, textElement, { r, g, b, a }) {
    try {
      textElement.text = textElement.text.replace(
        /color:rgba\([0-9,.]+\);/g,
        `color:rgba(${r},${g},${b},${a});`,
      );
      editorStore.setText(i, textElement.text);
    } catch {}
  }

  @bind
  parseColor(text) {
    const c = text
      .replace(/^.*color:rgba\(/, '')
      .replace(/\);.*$/, '')
      .split(',');

    return { r: parseFloat(c[0]), g: parseFloat(c[1]), b: parseFloat(c[2]), a: parseFloat(c[3]) };
  }

  @bind
  changeBackground(backgrounds, currentBackground, setBackground, addBackground) {
    if (!backgrounds) {
      this.setState({ ...this.state, changeBackground: false });
      return;
    }

    const { changeBackground } = this.state;
    this.setBackgroundP = setBackground;
    this.addBackgroundP = addBackground;
    this.setState({
      ...this.state,
      changeBackground: !changeBackground,
      backgrounds,
      currentBackground,
    });
  }

  @bind
  setBackground(id) {
    this.setBackgroundP(id);
    this.setState({ ...this.state, currentBackground: id });
  }

  @bind
  async addNewBackground(bg) {
    const { backgrounds, currentBackground } = await this.addBackgroundP(bg);
    this.setState({
      ...this.state,
      backgrounds,
      currentBackground,
    });
  }

  @bind
  toggleColorPicker(toggle = true) {
    if (!toggle) return this.setState({ ...this.state, displayColorPicker: false });
    const { displayColorPicker } = this.state;
    this.setState({ ...this.state, displayColorPicker: !displayColorPicker });
  }

  @bind
  @action
  openBackgroundUploadModal() {
    editorStore.modalType = ModalType.BACKGROUND_UPLOAD;
    editorStore.modalOpen = true;
    editorStore.addNewBackground = this.addNewBackground;
  }

  // render() {
  //   const { changeBackground, backgrounds, currentBackground, displayColorPicker } = this.state;

  //   if (changeBackground) {
  //     return (
  //       <>
  //         <span className="sidebar-title">Choose a background</span>
  //         <div className="sidebar-content">
  //           {Object.entries(backgrounds).map(([id, bg], i) => (
  //             <div className="item" onClick={() => this.setBackground(id)}>
  //               <img className={currentBackground === id ? 'selected' : null} src={bg} key={id} />
  //               {currentBackground === id && <span className="selected">Selected</span>}
  //             </div>
  //           ))}
  //           <div className="item" onClick={this.openBackgroundUploadModal}>
  //             <img className="empty" src={Object.values(backgrounds)[0]} />
  //             <span className="empty">Upload new</span>
  //           </div>
  //         </div>
  //       </>
  //     );
  //   }

  //   const isSelected = Object.keys(editorStore.selectedElement).length;

  //   if (!isSelected) {
  //     return null;
  //   }

  //   const { page } = editorStore.selectedElement;
  //   const textOnPageIndex = editorStore.selectedElement.i;
  //   const textElement = page.texts[textOnPageIndex];

  //   return (
  //     <>
  //       <span className="sidebar-title">Properties</span>
  //       {isSelected && (
  //         <div className="sidebar-content">
  //           <div className="item">
  //             <span>Font size</span>
  //             <div className="actions font-size">
  //               <button onClick={() => this.resizeFont(textOnPageIndex, textElement, -1)}>-</button>
  //               <span>{textElement.text.replace(/^.*font-size:/, '').replace(/px.*$/, '')}</span>
  //               <button onClick={() => this.resizeFont(textOnPageIndex, textElement, 1)}>+</button>
  //             </div>
  //           </div>
  //           <div className="item">
  //             <span>Font color</span>
  //             <div className="actions">
  //               <button onClick={this.toggleColorPicker}>Select color</button>
  //             </div>
  //           </div>

  //           {displayColorPicker && (
  //             <div className="item font-color">
  //               <CompactPicker
  //                 color={this.parseColor(textElement.text)}
  //                 onChange={(color) => this.recolorFont(textOnPageIndex, textElement, color.rgb)}
  //               />
  //             </div>
  //           )}
  //         </div>
  //       )}
  //     </>
  //   );
  // }

  render() {
    const isSelected = Object.keys(editorStore.selectedElement).length;

    if (!isSelected) {
      return (
        <>
          <span className="sidebar-title">Changes</span>
          <div className="sidebar-content">
            {editorStore.changes.map(({ page, text, textIndex }, i) =>
              !(editorStore.currentPage === page) ? null : (
                <div
                  className="item"
                  onMouseEnter={() => {
                    this.textIndex = textIndex;
                    editorStore.setText(textIndex, text);
                  }}
                  onMouseLeave={() => {
                    editorStore.resetText(textIndex);
                  }}
                  onMouseOver={() => {
                    if (textIndex === this.textIndex) editorStore.setText(textIndex, text);
                  }}
                >
                  <span className="text">
                    #{i.toString().padStart(3, '0')}
                    {'\t'}
                    {text.replace(/<(.|\n)*?>/g, '')}
                  </span>
                  <div className="actions">
                    <button
                      className="green"
                      onClick={() => {
                        editorStore.applyText(textIndex, text);
                        editorStore.changes.splice(i, 1);
                      }}
                    >
                      Approve
                    </button>
                    <button
                      className="red"
                      onClick={() => {
                        editorStore.resetText(textIndex);
                        editorStore.rejectText(textIndex, text);
                        editorStore.changes.splice(i, 1);
                      }}
                    >
                      Reject
                    </button>
                  </div>
                </div>
              ),
            )}
          </div>
        </>
      );
    }

    return null;

    // const { page } = editorStore.selectedElement;
    // const textOnPageIndex = editorStore.selectedElement.i;
    // const textElement = page.texts[textOnPageIndex];

    // return (
    //   <>
    //     <span className="sidebar-title">Properties</span>
    //     {isSelected && (
    //       <div className="sidebar-content">
    //         <div className="item">
    //           <span>Font size</span>
    //           <div className="actions font-size">
    //             <button onClick={() => this.resizeFont(textOnPageIndex, textElement, -1)}>-</button>
    //             <span>{textElement.text.replace(/^.*font-size:/, '').replace(/px.*$/, '')}</span>
    //             <button onClick={() => this.resizeFont(textOnPageIndex, textElement, 1)}>+</button>
    //           </div>
    //         </div>
    //         <div className="item">
    //           <span>Font color</span>
    //           <div className="actions">
    //             <button onClick={this.toggleColorPicker}>Select color</button>
    //           </div>
    //         </div>

    //         {displayColorPicker && (
    //           <div className="item font-color">
    //             <CompactPicker
    //               color={this.parseColor(textElement.text)}
    //               onChange={(color) => this.recolorFont(textOnPageIndex, textElement, color.rgb)}
    //             />
    //           </div>
    //         )}
    //       </div>
    //     )}
    //   </>
    // );
  }
}

export default Sidebar;

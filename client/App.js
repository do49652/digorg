import React from 'react';
import { bind } from 'decko';
import Editor from './pages/editor';
import List from './pages/list';
import UploadForm from './components/UploadForm';
import './style.scss';

class App extends React.Component {
  state = {
    guid: null,
  };

  @bind
  setGuid(guid) {
    this.setState({ guid });
  }

  @bind
  exitEditor() {
    this.setState({ guid: null });
  }

  render() {
    const { guid } = this.state;

    return (
      <>
        {!guid && (
          <div>
            <UploadForm />
            <List setGuid={this.setGuid} />
          </div>
        )}

        {guid && <Editor guid={guid} exitEditor={this.exitEditor} />}
      </>
    );
  }
}

export default App;

import Shell from 'node-powershell';
import { dirname, basename } from 'path';
import { existsSync } from 'fs';
import rimraf from 'rimraf';
import { promisify } from 'util';

const ps = new Shell({
  executionPolicy: 'Bypass',
  noProfile: true,
});

export default async (path) => {
  if (existsSync(`${dirname(path)}/html`)) await promisify(rimraf)(`${dirname(path)}/html`);

  ps.addCommand(`cd ${dirname(path)}`);
  ps.addCommand(
    `& "C:\\Program Files\\LibreOffice\\program\\soffice" --convert-to pdf ${basename(path)}`,
  );
  ps.addCommand(`& "..\\..\\utils\\pdftohtml.exe" ${basename(path.replace(/\..+/, '.pdf'))} html`);

  try {
    await ps.invoke();
    return { isSuccess: true };
  } catch (err) {
    if (existsSync(`${dirname(path)}/html/index.html`))
      return { isSuccess: true, path: `${dirname(path)}/html/index.html` };
    return { isSuccess: false };
  }
};

import { readdirSync, readFileSync, writeFileSync } from 'fs';

export default async (path) => {
  const files = readdirSync(path)
    .filter((x) => x.includes('page') && x.endsWith('.html'))
    .map((x) => `${path}/${x}`);

  for (const file of files) {
    let text = readFileSync(file, 'utf-8');
    text = text.replace(
      /@font-face\s+\{\s+font-family:\s+ff[^;]*;\s+src:\s+url\("[^t]*ttf"\);\s+\}/g,
      '',
    );
    text = text.replace(/class="txt"/g, 'class="txt" contenteditable="true"');
    writeFileSync(file, text, 'utf-8');
  }
};

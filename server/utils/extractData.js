import fs from 'fs';
import { join } from 'path';
import rimraf from 'rimraf';
import { promisify } from 'util';

const parseInteger = (str) => {
  try {
    return parseInt(str);
  } catch {
    return null;
  }
};

const pngToBase64 = (filepath) => {
  try {
    const data = fs.readFileSync(filepath);
    const base64Image = new Buffer(data, 'binary').toString('base64');
    return `data:image/png;base64,${base64Image}`;
  } catch {
    return null;
  }
};

export default async (directory) => {
  const files = fs.readdirSync(directory);
  const pages = [{}];
  const backgrounds = {};

  for (const file of files) {
    const isBackground = file.endsWith('.png');
    const isHtml = file.endsWith('.html');

    if (!isBackground && !isHtml) continue;

    const pageNumber = parseInteger(file.replace(/[^0-9]/g, ''));
    if (!pageNumber) continue;

    if (!pages[pageNumber]) {
      pages[pageNumber] = {
        background: {
          id: 0,
          width: 0,
          height: 0,
        },
        texts: [],
      };
    }

    if (isBackground) {
      const image = pngToBase64(join(directory, file));
      const existingImage = Object.entries(backgrounds).find(([key, value]) => value === image);

      if (existingImage) {
        pages[pageNumber].background.id = parseInteger(existingImage[0]);
      } else {
        backgrounds[pageNumber] = image;
        pages[pageNumber].background.id = parseInteger(pageNumber);
      }
    } else if (isHtml) {
      const html = fs.readFileSync(join(directory, file), { encoding: 'utf-8' });

      if (!pages[pageNumber].background.width) {
        const matchWidth = html.match(/width="([^"]+)"/);
        if (matchWidth) pages[pageNumber].background.width = parseInteger(matchWidth[1]);
      }

      if (!pages[pageNumber].background.height) {
        const matchHeight = html.match(/height="([^"]+)"/);
        if (matchHeight) pages[pageNumber].background.height = parseInteger(matchHeight[1]);
      }

      pages[pageNumber].texts = html.match(/<div class="txt".*div>/g).map((text) => {
        let left, top;

        const matchLeft = text.match('left:([^p]+px)');
        if (matchLeft) left = matchLeft[1];

        const matchTop = text.match('top:([^p]+px)');
        if (matchTop) top = matchTop[1];

        text = text
          .replace(/^[^>]+>/, '')
          .replace(/<\/div>$/, '')
          .replace(/ id="[^"]+" /g, ' ');

        return { left, top, text };
      });
    }
  }

  await promisify(rimraf)(directory);

  return { pages, backgrounds };
};

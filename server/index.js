import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';

import './db';

import {
  findPresentation,
  addChange,
  addComment,
  getPresentationNames,
  createPresentation,
  removeChange,
  applyChange,
} from './db/presentation';

import uploader from './utils/uploader';
import convert from './utils/convert';
import guid from './utils/guid';
import extractData from './utils/extractData';

var app = express();
app.use(express.static('dist'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '50mb' }));

app.get('/p/:guid', async (req, res) => {
  const { guid } = req.params;
  const data = await findPresentation({ guid });
  res.json(data);
});

app.post('/change/:guid', async (req, res) => {
  const { guid } = req.params;
  const { page, textIndex, text } = req.body;
  try {
    await addChange({ guid }, { page, textIndex, text });
    res.json({ isSuccess: true });
  } catch (err) {
    res.json({ isSuccess: false, message: err.toString() });
  }
});

app.delete('/change/:guid', async (req, res) => {
  const { guid } = req.params;
  const { page, textIndex, text } = req.body;
  try {
    await removeChange({ guid }, { page, textIndex, text });
    res.json({ isSuccess: true });
  } catch (err) {
    res.json({ isSuccess: false, message: err.toString() });
  }
});

app.put('/change/:guid', async (req, res) => {
  const { guid } = req.params;
  const { page, textIndex, text } = req.body;
  try {
    await applyChange({ guid }, { page, textIndex, text });
    res.json({ isSuccess: true });
  } catch (err) {
    res.json({ isSuccess: false, message: err.toString() });
  }
});

app.post('/comment/:guid', async (req, res) => {
  const { guid, page, textIndex, text } = req.params;
  try {
    await addComment({ guid }, { page, textIndex, text });
    res.json({ isSuccess: true });
  } catch (err) {
    res.json({ isSuccess: false, message: err.toString() });
  }
});

app.get('/files', async (req, res) => {
  const data = await getPresentationNames();
  res.json(data || []);
});

app.post('/upload', (req, res) => {
  uploader(req, res, async (err) => {
    if (err) return res.status(500).redirect('/');

    const fileGuid = guid();
    const path = req.file.path.replace(/([\\/]+)([^\\/]+)$/, `$1${fileGuid}$1$2`);
    fs.mkdirSync(path.replace(/[\\/]+[^\\/]+$/, ''), { recursive: true });
    fs.renameSync(req.file.path, path);

    const { isSuccess, path: htmlPath } = await convert(path);
    if (!isSuccess) res.status(500).redirect('/');

    const data = await extractData(htmlPath.replace(/[\\/]+[^\\/]+$/, ''));

    await createPresentation({
      ...data,
      guid: fileGuid,
      name: req.file.filename.replace(/\.[^.]+$/, ''),
      changes: [],
      comments: [],
    });

    res.redirect('/');
  });
});

const port = process.env.PORT || 3000;
app.listen(port, console.log(`Server is actually at http://localhost:${port}`));

export default app;

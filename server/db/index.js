import mongoose from 'mongoose';

const productionUrl = 'mongodb://localhost:27017/digobr';
const localUrl = 'mongodb://localhost:27017/digobr';

export const dbUrl = process.env.NODE_ENV === 'production' ? productionUrl : localUrl;

let connection = null;
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(dbUrl).then(() => {
  connection = mongoose.connection;
});

export default connection;

import mongoose, { Schema } from 'mongoose';

const Presentation = mongoose.model(
  'Presentation',
  new Schema({
    name: String,
    guid: String,
    backgrounds: Object,
    pages: [
      {
        background: {
          id: String, // TODO: Maybe change to number
          width: Number,
          height: Number,
        },
        texts: [
          {
            left: String,
            top: String,
            text: String,
            // fontSize: String, // TODO: Maybe
            // color: String,
          },
        ],
      },
    ],
    changes: [
      {
        page: Number,
        textIndex: Number,
        text: String,
      },
    ],
    comments: [
      {
        page: Number,
        textIndex: Number,
        text: String,
      },
    ],
  }),
);

export const getPresentationNames = () =>
  new Promise((resolve) => {
    Presentation.find({}, { guid: 1, name: 1 }, (err, p) => resolve(p));
  });

export const findPresentation = (query) =>
  new Promise((resolve) => {
    Presentation.findOne(query, (err, p) => resolve(p));
  });

export const createPresentation = (p) =>
  new Promise((resolve) => {
    const newP = new Presentation({ ...p });
    newP.save(() => resolve(newP));
  });

export const addChange = (query, change) =>
  new Promise((resolve) => {
    findPresentation(query).then((p) => {
      p.changes.push(change);
      p.save(resolve);
    });
  });

export const removeChange = (query, change) =>
  new Promise((resolve) => {
    findPresentation(query).then((p) => {
      const i = p.changes.findIndex(
        ({ page, textIndex, text }) =>
          change.page === page && change.textIndex === textIndex && change.text === text,
      );
      p.changes.splice(i, 1);
      p.markModified('anything');
      p.save(resolve);
    });
  });

export const applyChange = (query, change) =>
  new Promise((resolve) => {
    findPresentation(query).then((p) => {
      const i = p.changes.findIndex(
        ({ page, textIndex, text }) =>
          change.page === page && change.textIndex === textIndex && change.text === text,
      );
      p.changes.splice(i, 1);

      p.pages[change.page].texts[change.textIndex].text = change.text;

      p.markModified('anything');
      p.save(resolve);
    });
  });

export const addComment = (query, comment) =>
  new Promise((resolve) => {
    findPresentation(query).then((p) => {
      p.comments.push(comment);
      p.save(resolve);
    });
  });
